��          �      <      �     �  A   �                    $     2     :     Y     j     s     x     �     �  	   �     �  <   �  �  �     �  7   �     "     .     :     H     Z     _     }     �     �     �     �     �     �     �  <   �         	                                                                                        
    ACF: Country Field Adds a custom country field to ACF with extensive output options. Alpha2 Code Alpha3 Code Array Calling Codes Country Error! Please select a country Jurgen Oldenburg Lat Long Name Numeric Code Region Return value Subregion http://www.jold.nl https://bitbucket.org/joldinteractive/jold-acf-country-field Project-Id-Version: ACF: Country Field
POT-Creation-Date: 2017-09-22 17:39+0200
PO-Revision-Date: 2017-09-22 17:41+0200
Last-Translator: Jurgen Oldenburg <info@jold.nl>
Language-Team: Jurgen Oldenburg <info@jold.nl>
Language: nl_NL
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.12
X-Poedit-Basepath: ..
X-Poedit-WPHeader: jold-acf-country-field.php
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 ACF: Land Veld Voeg een 'land' veld toe met uitgebreide output opties. Alpha2 Code Alpha3 Code Array (reeks) Telefoon landcode Land Error! Selecteer aub een land Jurgen Oldenburg GPS Coordinaat Naam Numerieke Code Regio Output waarde Subregio http://www.jold.nl https://bitbucket.org/joldinteractive/jold-ACF-Country-Field 
