# ACF Country Field

### Description

Adds a custom country field to ACF with extensive output options to the Advanced Custom Fields Pro plugin.


## Installation:

Add repository to your local composer file:

    {
        "type": "composer",
        "url" : "https://packages.jold.nl/satispress/"
    }



Add the plugin as a dependency:

    "joldnl/jold-acf-country-field": "~1.0",



Update composer by running:

    $ composer update.


Or just upload the zip file and install it manually as you normally would.


## Usage:

Add the country field to a field group.
Select the out put type. There are several pre-defined fields, like 'Country Name',
several country codes, or return the complete country dataset:

```
[
    'name'              => string 'Botswana',
    'topLevelDomain'    => [
        0 => string '.bw',
    ],
    'alpha2Code'        => string 'BW',
    'alpha3Code'        => string 'BWA',
    'callingCodes'      => [
        0 => string '267',
    ],
    'capital'           => string 'Gaborone',
    'altSpellings'      => [
        0 => string 'BW',
        1 => string 'Republic of Botswana',
        2 => string 'Lefatshe la Botswana',
    ],
    'region'            => string 'Africa',
    'subregion'         => string 'Southern Africa',
    'population'        => int 2141206
    'latlng'            => [
        0 => float -22
        1 => float 24
    ],
    'demonym'           => string 'Motswana',
    'area'              => float 582000
    'gini'              => float 61
    'timezones'         => [
        0 => string 'UTC+02:00',
    ],
    'borders'           => [
        0 => string 'NAM',
        1 => string 'ZAF',
        2 => string 'ZMB',
        3 => string 'ZWE',
    ],
    'nativeName'        => string 'Botswana',
    'numericCode'       => string '072',
    'currencies'        => [
        0 => [
            'code'      => string 'BWP',
            'name'      => string 'Botswana pula',
            'symbol'    => string 'P',
        ],
    ],
    'languages'         => [
        0 => [
            'iso639_1'      => string 'en',
            'iso639_2'      => string 'eng',
            'name'          => string 'English',
            'nativeName'    => string 'English',
        ],
        1 => [
            'iso639_1'      => string 'tn',
            'iso639_2'      => string 'tsn',
            'name'          => string 'Tswana',
            'nativeName'    => string 'Setswana',
        ],
    ],
    'translations'      => [
        'de' => string 'Botswana',
        'es' => string 'Botswana',
        'fr' => string 'Botswana',
        'ja' => string 'ボツワナ',
        'it' => string 'Botswana',
        'br' => string 'Botsuana',
        'pt' => string 'Botsuana',
    ],
    'flag'              => string 'https://restcountries.eu/data/bwa.svg',
    'regionalBlocs'     => [
        0 => [
            'acronym'       => string 'AU',
            'name'          => string 'African Union',
            'otherAcronyms' =>
        [
    ],
    'otherNames'        => [
        0 => string 'الاتحاد الأفريقي',
        1 => string 'Union africaine',
        2 => string 'União Africana',
        3 => string 'Unión Africana',
        4 => string 'Umoja wa Afrika',
    ],
]

```

All country data in the countries.json file was generated with https://restcountries.eu/
