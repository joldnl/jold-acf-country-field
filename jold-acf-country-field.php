<?php

/*
    Plugin Name:           ACF: Country Field
    Description:           Adds a custom country field to ACF with extensive output options.
    Version:               1.0.4
    Plugin URI:            https://bitbucket.org/joldnl/jold-acf-country-field
    Bitbucket Plugin URI:  https://bitbucket.org/joldnl/jold-acf-country-field
    Author:                Jurgen Oldenburg
    Author URI:            http://www.jold.nl
    License:               GPLv2 or later
    License URI:           http://www.gnu.org/licenses/gpl-2.0.html
*/



// Enqueue css for input type=tel element
function jold_acf_country_field_enqueue($hook) {

    if ( 'post.php' != $hook ) {

        return;

    }

}


// Include phone field type for ACF5
add_action( 'acf/include_field_types',  'jold_acf_country_field' );

function jold_acf_country_field( $version ) {


    class acf_field_country_field extends acf_field {



        /*
         *  __construct
         *
         *  Main construct function, fired on load
         *
         *  @type    function
         *  @date    06/12/2015
         *  @since   1.0
         *
         *  @param   void
         *  @return  void
         */
        function __construct() {
            $this->name     = 'country';                            // Field name
            $this->label    = __('Country', 'acf-country-field');   // Field label
            $this->category = 'basic';                              // Field category
            $this->defaults = array(                                // Field defaults
                'multiple'  => 0,
            );

            $this->l10n = array(
                'error'     => __('Error! Please select a country', 'acf-country-field'),
            );

            // Set textdomain
            load_plugin_textdomain( 'acf-country-field', false, dirname( plugin_basename(__FILE__) ) . '/lang/' );

            // Add actions
            add_action( 'acf/include_field_types',  array( $this, 'jold_acf_country_field' ) );

            // Construct this class in parent class (acf_field)
            parent::__construct();

        }



        /*
         *  render_field_settings
         *
         *  Get, format and return the world country dataset
         *  This dataset is originative from https://restcountries.eu/rest/v2/all
         *
         *  @type     function
         *  @date     06/12/2015
         *  @since    1.0
         *
         *  @param    void
         *  @return   array  Array wit all country details
         */
        function country_data() {

            $file       =  plugin_dir_path(__FILE__) .'countries.json';

            if ( file_exists($file) ) {
                $josn   = file_get_contents( $file );
            }

            if ( isset($josn) && !empty($josn) ) {

                $countries  = json_decode( $josn, true );
                return $countries;

            }

            return false;

        }



        /*
         *  render_field_settings
         *
         *  ACF Callback: Render the field settings in the admin screen
         *
         *  @type     function
         *  @date     26/)5/2017
         *  @since    1.0
         *
         *  @param    array    $field    The field details array
         *  @return   void
         */
        function render_field_settings( $field ) {

            // Set output settings
            acf_render_field_setting( $field, array(
                'label'          => __('Return value','acf-country-field'),
                'type'           => 'select',
                'name'           => 'output_format',
                'choices'        => array(
                    'array'         => __( 'Array',         'acf-country-field' ),
                    'name'          => __( 'Name',          'acf-country-field' ),
                    'alpha2Code'    => __( 'Alpha2 Code',   'acf-country-field' ),
                    'alpha3Code'    => __( 'Alpha3 Code',   'acf-country-field' ),
                    'callingCodes'  => __( 'Calling Codes', 'acf-country-field' ),
                    'region'        => __( 'Region',        'acf-country-field' ),
                    'subregion'     => __( 'Subregion',     'acf-country-field' ),
                    'latlng'        => __( 'Lat Long',      'acf-country-field' ),
                    'numericCode'   => __( 'Numeric Code',  'acf-country-field' ),
                ),
                'layout'          =>    'vertical',
            ));

        }



        /*
         *  format_value
         *
         *  ACF Callback: Format the frontend output of the phone field
         *
         *  @type     function
         *  @date     26/05/2017
         *  @since    1.0
         *
         *  @param    string     $value      The field value from the database
         *  @param    int        $post_id    The post ip the field value belongs to
         *  @param    array      $field      Field details
         *  @return   mixed                  The formatted field value output
         */
        function format_value( $value, $post_id, $field ) {

            $countries      = $this->country_data();
            $country_data   = null;

            // Get the coutry details from the world dataset
            foreach ( $countries as $country ) {
                if ( $country['alpha3Code'] == $value ) {
                    $country_data = $country;
                }
            }

            // Return all data if no field is passed
            if( empty($value) ) {
                return $country_data;
            }

            if( $field['output_format'] != 'array' ) {
                return $country_data[ $field['output_format'] ];      // Return the selected field
            } else {
                return $country_data;                                 // Return all te country data
            }


        }



        /*
         *  render_field
         *
         *  ACF Callback: Render admin input field
         *
         *  @type     function
         *  @date     26/05/2017
         *  @since    1.0
         *
         *  @param    array    $field     The registered field details
         *  @return   mixed               Rendered html output of the title field
         */
        function render_field( $field ) {
?>
<div class="acf-input-wrap">
    <select class="" name="<?php echo esc_attr( $field['name'] ); ?>">
        <?php foreach ($this->country_data() as $country) : ?>
            <option value="<?php echo esc_attr( $country['alpha3Code'] ); ?>"<?php if ( esc_attr( $country['alpha3Code'] ) == esc_attr( $field[ 'value' ] ) ) { echo 'selected="selected"'; } ?>><?php echo esc_attr( $country['name'] ); ?> (<?php echo esc_attr( $country['alpha3Code'] ); ?>)</option>
        <?php endforeach; ?>
    </select>
</div>
<?php
        }


    }

    new acf_field_country_field();

}

?>
